package ru.itgirl;

import java.util.Scanner;

public class App {
    enum WeekdaysEnum {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY;
    }
    public static void main(String[] args) {
        System.out.println("Введите день недели на английском:");
        Scanner input = new Scanner(System.in);
        String enWeekday = input.nextLine();
        try {
            WeekdaysEnum day = WeekdaysEnum.valueOf(enWeekday.toUpperCase());
            switch (day) {
                case MONDAY:
                    System.out.println("Сегодня понедельник");
                    break;
                case TUESDAY:
                    System.out.println("Сегодня вторник");
                    break;
                case WEDNESDAY:
                    System.out.println("Сегодня среда");
                    break;
                case THURSDAY:
                    System.out.println("Сегодня четверг");
                    break;
                case FRIDAY:
                    System.out.println("Сегодня пятница");
                    break;
                case SATURDAY:
                    System.out.println("Сегодня суббота");
                    break;
                case SUNDAY:
                    System.out.println("Сегодня воскресенье");
                    break;
            }
        } catch (IllegalArgumentException e) {
            System.out.println("День недели введен неверно");
        }
    }
}
